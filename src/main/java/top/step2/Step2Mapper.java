package top.step2;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;
import static utils.ComUtils.split;

public class Step2Mapper extends Mapper<Text, Text, Text, Text> {


    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {

        String[] list = split(value.toString(),"\n");
        Map<String,Integer> data = new HashMap<>();
        for (String line : list) {
            if (!"".equals(line)){
                String[] word_sum = split(line,"\t");
                data.put(word_sum[0],Integer.parseInt(word_sum[1]));
            }
        }

        HashMap<String, Integer> map = data.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(comparingByValue()))
                .limit(10)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (m1, m2) -> m2, HashMap::new));

        for (String k : map.keySet()) {
            context.write(new Text("key"),new Text(k + "\t" + map.get(k)));
        }
    }
}

