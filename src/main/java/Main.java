import top.step1.Step1Driver;
import top.step2.Step2Driver;

public class Main {
    public static void main(String[] args) throws Exception {
        String in1 = "data/input/nationalgeographic_1.txt";
        String in2 = "data/input/nationalgeographic_2.txt";
        String in3 = "data/input/nationalgeographic_3.txt";

        String out1 = "data/tmp1";
        String out2 = "data/tmp2";
        String out3 = "data/tmp3";

        String input = "data/tmp[1-3]";
        String output = "data/output";

        // -------------------分n个-------------------
        Step1Driver.run(new String[]{in1,out1});
        Step1Driver.run(new String[]{in2,out2});
        Step1Driver.run(new String[]{in3,out3});
        Step2Driver.run(new String[]{input,output});

        //---------------------一个整体----------------------
        Step1Driver.run(new String[]{"data/input","data/tmp"});
        Step2Driver.run(new String[]{"data/tmp",output});


    }
}
