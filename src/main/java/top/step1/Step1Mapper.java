package top.step1;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static utils.ComUtils.split;

public class Step1Mapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    List<String> stop_word = new ArrayList<>();
    String[] _stop = new String[]{"in","of","a","and","as","are","at","are","with","an","to","the","that","for","your","they","these","from","on","i","is"};
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        stop_word.addAll(Arrays.asList(_stop));
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // 分词前处理标点, 具体情况具体处理
        String line = value.toString().toLowerCase()
                .replace(",","")
                .replace(".","")
                .replace("(","")
                .replace(")","")
                .replace("“","")
                .replace("\"","");
        // 分词
        String[] words = split(line," ");
        for (String word : words) {
            // 每个单词写出
            word = word.replace(" ","");
            if (!stop_word.contains(word) && !StringUtils.isNumeric(word)) context.write(new Text(word),new IntWritable(1));
        }
    }
}

