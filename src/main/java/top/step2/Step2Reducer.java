package top.step2;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static utils.ComUtils.split;

public class Step2Reducer extends Reducer<Text, Text, Text, NullWritable> {


    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        List<String[]> res = new ArrayList<>();
        for (Text value : values) {
            String[] str = split(value.toString(),"\t");
            if (res.size() < 10){
                res.add(str);
            }else{
                res.sort(Comparator.comparingInt(o -> Integer.parseInt(o[1])));

                if (Integer.parseInt(str[1]) > Integer.parseInt(res.get(0)[1])){
                    res.remove(0);
                    res.add(str);
                    System.out.println("");
                }
            }

        }
        res.sort(Comparator.comparingInt(o -> Integer.parseInt(o[1])));
        Collections.reverse(res);
        for (String[] re : res) {
            context.write(new Text(re[0] + "\t" + re[1]),NullWritable.get());
        }
    }
}
