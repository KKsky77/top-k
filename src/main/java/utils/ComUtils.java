package utils;

import org.apache.commons.lang3.StringUtils;

public class ComUtils {
    public static String[] split(String str, String split) {
        return StringUtils.splitByWholeSeparatorPreserveAllTokens(str, split);
    }
}
